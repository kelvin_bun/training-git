create table tamu (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nama varchar(255) not null,
  email varchar (255) not null
);

create table comment (
    id INT PRIMARY KEY AUTO_INCREMENT,
    id_tamu INT not null,
    tanggal DATETIME not null,
    pesan TEXT not null
);