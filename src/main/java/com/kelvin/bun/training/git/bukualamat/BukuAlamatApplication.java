package com.kelvin.bun.training.git.bukualamat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BukuAlamatApplication {

	public static void main(String[] args) {
		SpringApplication.run(BukuAlamatApplication.class, args);
	}

}
